# README

```
ln -s /etc/dotfiles $DOTFILES
sudo chown -R roland:users /etc/dotfiles/
```

```
# AS ROOT!
# make channels
export NIXOS_VERSION=19.09
nix-channel --add "https://nixos.org/channels/nixos-${NIXOS_VERSION}" nixos
nix-channel --add "https://github.com/rycee/home-manager/archive/release-${NIXOS_VERSION}.tar.gz" home-manager
nix-channel --add "https://nixos.org/channels/nixpkgs-unstable" nixpkgs-unstable
```

```
echo "import /etc/dotfiles \"$$(hostname)\"" >/mnt/etc/nixos/configuration.nix
```
