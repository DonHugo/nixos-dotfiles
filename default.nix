# default.nix

device: username: fullName:
{ pkgs, options, lib, config, ... }:
{
  networking.hostName = lib.mkDefault device;

  imports = [
    # Include the results of the hardware scan.
    ../nixos/hardware-configuration.nix
    ./modules
    # Host configuration
    "${./hosts}/${device}"
  ];

  # NixOS
  nix.autoOptimiseStore = true;
  nix.nixPath = options.nix.nixPath.default ++ [
    # So we can use absolute import paths
    "bin=/etc/dotfiles/bin"
    "config=/etc/dotfiles/config"
    "modules=/etc/dotfiles/modules"
    "custompkgs=/etc/dotfiles/packages"
  ];
  # Add custom packages & unstable channel, so they can be accessed via pkgs.*
  nixpkgs.overlays = import ./overlays.nix;
  # Allow unfree packages
  nixpkgs.config.allowUnfree = true;

  environment.systemPackages = with pkgs; [
    # Necessities
    coreutils
    git
    gnumake
    htop
    killall
    nano
    nix-index
    sshfs
    unzip
    wget
    zstd

    # Support for more filesystems
    btrfs-progs
    dosfstools
    exfat
    f2fs-tools
    jfsutils
    mtools
    nilfs-utils
    ntfs3g
    reiserfsprogs
    udftools
    xfsprogs
  ];

  # Environment
  environment.shellAliases = {
    nix-env = "NIXPKGS_ALLOW_UNFREE=1 nix-env";
    nix-shell = ''NIX_PATH="nixpkgs-overlays=/etc/dotfiles/overlays.nix:$NIX_PATH" nix-shell'';
    # dots = "make -C ~/.dotfiles";
  };

  # User settings
  my.username = username;
  my.user = {
    description = fullName;
    isNormalUser = true;
    createHome = true;
    home = ("/home/${username}");
    extraGroups = [ "wheel" "video" "networkmanager" ];
  };

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "20.03"; # Did you read the comment?
}
