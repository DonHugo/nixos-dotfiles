# Desktop

{ config, lib, pkgs, ... }:

let custompkgs = import <custompkgs> { };
in {

  imports = [
    # System specific configuration
    ./hardware-configuration.nix
    # tkg-kernel: Needs compiling!
    # ./tkg-kernel.nix
    # Common settings
    ../personal.nix
    # Uncategorized packages
    ./misc.nix
    # User settings
    # ./user.nix
  ];

  modules = {
    desktop = {
      gnome.enable = true;

      browsers.vivaldi.enable = true;

      apps.riot.enable = true;
      apps.zoom.enable = true;
      apps.keepass.enable = true;
      apps.nextcloud.enable = true;
      # apps.redshift.enable = true;

      # gaming.enable = true;

      media.clementine.enable = true;
      media.vlc.enable = true;

      office.libreoffice.enable = true;
    };

    editors = {
      default = "nano";
      emacs.enable = true;
    };

    fonts.enable = true;

    hardware = {
      hpPrinter.enable = true;
      xpadneo.enable = true;
      broadcom-bt-firmware.enable = true;
    };

    shell = {
      direnv.enable = true;
      fish.enable = true;
    };
  };

  # Backups
  # Don't forget to copy systemd service in home and borgmatic config
  # files in /etc/borgmatic.d
  modules.tools = { borgmatic.enable = true; };

  # CPU Governor
  powerManagement.cpuFreqGovernor = "ondemand";

  # Firmware updates
  services.fwupd.enable = true;

  # Select internationalisation properties.
  i18n.defaultLocale = "de_DE.UTF-8";
  console.font = "Lat2-Terminus16";
  console.keyMap = "us";

  # Keyboard and touchpad
  services.xserver = { layout = "eu"; };

  # Time zone.
  time.timeZone = "Europe/Berlin";

  # Firewall
  # Not necessary at home
  networking.firewall.enable = false;

  # I/O Scheduler
  boot.initrd.kernelModules = [ "bfq" ];
  services.udev.extraRules = ''
    # set scheduler for NVMe
    ACTION=="add|change", KERNEL=="nvme[0-9]*", ATTR{queue/scheduler}="none"
    # set scheduler for SSD and eMMC
    ACTION=="add|change", KERNEL=="sd[a-z]|mmcblk[0-9]*", ATTR{queue/rotational}=="0", ATTR{queue/scheduler}="mq-deadline"
    # set scheduler for rotating disks
    ACTION=="add|change", KERNEL=="sd[a-z]", ATTR{queue/rotational}=="1", ATTR{queue/scheduler}="bfq"
  '';
}
