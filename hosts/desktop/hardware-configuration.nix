# Machine specific stettings

{ config, lib, pkgs, ... }:

let unstablepkgs = import <nixos-unstable> { };
in {
  # Packages
  environment.systemPackages = with pkgs; [ ];

  # Hardware and system settings
  hardware.enableRedistributableFirmware = true;

  # CPU
  # Maximum number of parallel build jobs
  nix.maxJobs = lib.mkDefault 32;
  # Microcode
  hardware.cpu.amd.updateMicrocode = true;

  # SSD
  # Fstrim
  services.fstrim.enable = true;
  # Mounts
  fileSystems."/".options = [ "noatime" ];

  fileSystems."/run/media/roland/Data" = {
    device = "/dev/disk/by-uuid/044fd928-c9b4-4131-9986-a76464a9bf07";
    fsType = "btrfs";
    options = [
      "rw"
      "noatime"
      "space_cache"
      "compress=zstd"
      "commit=120"
      "autodefrag"
      "nofail"
    ];
  };

  fileSystems."/run/media/roland/Backup" = {
    device = "/dev/disk/by-uuid/75D6F18346513DA0";
    fsType = "ntfs-3g";
    options = [ "uid=1000" "gid=100" "dmask=022" "fmask=133" "nofail" ];
  };

  # GPU
  services.xserver.videoDrivers = [ "amdgpu" ];

  # OpenGL
  hardware.opengl = {
    enable = true;
    # package = unstablepkgs.mesa.drivers;
    driSupport32Bit = true;
    # package32 = unstablepkgs.pkgsi686Linux.mesa.drivers;
    extraPackages = with pkgs; [ libvdpau libvdpau-va-gl ];
    extraPackages32 = with pkgs; [ libvdpau libvdpau-va-gl ];
  };

  # Audio

  # Sensors
  boot.kernelModules = [ "nct6775" ];

  # Networking
  # Wifi
  networking.networkmanager.enable = true;
  networking.networkmanager.wifi.powersave = false;

  # The global useDHCP flag is deprecated, therefore explicitly set to false here.
  # Per-interface useDHCP will be mandatory in the future, so this generated config
  # replicates the default behaviour.
  networking.useDHCP = false;
  networking.interfaces.enp34s0.useDHCP = true;
  networking.interfaces.wlo1.useDHCP = true;
}
