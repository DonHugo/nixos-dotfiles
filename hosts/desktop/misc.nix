{ config, options, lib, pkgs, ... }:

{
  environment.systemPackages = with pkgs; [
    xsettingsd
    displaycal
    freerdp
    remmina
    gnucash
    xorg.xhost
    lm_sensors
    calibre
    nixfmt
    gparted
    nix-prefetch-scripts
    usbutils
    hdparm
    simplescreenrecorder
    unstable.riot-desktop
  ];

  virtualisation.virtualbox = {
    host.enable = true;
    host.enableExtensionPack = true;
  };
  users.extraGroups.vboxusers.members = [ "roland" ];

  virtualisation.lxd.enable = true;

  my.user = { extraGroups = [ "lxd" "disk" ]; };

  # Flatpak
  services.flatpak.enable = true;
  xdg.portal.enable = true;

  # ln -sv /etc/fonts/fonts.conf ~/.var/app/org.gnome.Evolution/config/fontconfig/fonts.conf

}
