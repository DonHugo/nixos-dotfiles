{ config, lib, pkgs, ... }:

{
  boot.kernelPackages = let
    linux_tkg_pkg = { fetchurl, buildLinux, ... }@args:

      buildLinux (args // rec {
        version = "5.6.13";
        modDirVersion = version;

        src = fetchurl {
          url = "mirror://kernel/linux/kernel/v5.x/linux-${version}.tar.xz";
          sha256 = "11zriz0jwqizv0pq0laql0svsnspdfnxqykq70v22x39iyfdf9gi";
        };

        ignoreConfigErrors = true;

        structuredExtraConfig = with import (pkgs.path + "/lib/kernel.nix") {
          inherit lib;
          inherit version;
        }; {
          ## Set some -tkg defaults
          DYNAMIC_FAULT = no;
          DEFAULT_FQ_CODEL = no;
          DEFAULT_CAKE = yes;
          # NR_TTY_DEVICES = 63;
          RAID6_USE_PREFER_GEN = yes;
          NTP_PPS = no;
          CRYPTO_LZ4 = yes;
          CRYPTO_LZ4HC = yes;
          LZ4_COMPRESS = yes;
          LZ4HC_COMPRESS = yes;
          # RCU_BOOST_DELAY = 0;
          CMDLINE_BOOL = yes;
          # CMDLINE = "intel_pstate=passive";
          CMDLINE_OVERRIDE = no;

          ## Inject cpuopts options
          MK8SSE3 = no;
          MK10 = no;
          MBARCELONA = no;
          MBOBCAT = no;
          MJAGUAR = no;
          MBULLDOZER = no;
          MPILEDRIVER = no;
          MSTEAMROLLER = no;
          MEXCAVATOR = no;
          MZEN = no;
          MZEN2 = no;
          MATOM = no;
          MNEHALEM = no;
          MWESTMERE = no;
          MSILVERMONT = no;
          MSANDYBRIDGE = no;
          MIVYBRIDGE = no;
          MHASWELL = no;
          MBROADWELL = no;
          MSKYLAKE = no;
          MSKYLAKEX = no;
          MCANNONLAKE = no;
          MICELAKE = no;
          MGOLDMONT = no;
          MGOLDMONTPLUS = no;
          MCASCADELAKE = no;

          # MuQSS default config
          # SCHED_MUQSS = yes;
          # PDS default config
          SCHED_PDS = yes;
          # Disable CFS
          FAIR_GROUP_SCHED = no;
          CFS_BANDWIDTH = no;
          CGROUP_CPUACCT = no;
          SCHED_AUTOGROUP = no;

          # zenify
          ZENIFY = yes;

          # compiler optimization level
          # O2
          CC_OPTIMIZE_FOR_PERFORMANCE_O3 = no;
          # O3
          # CC_OPTIMIZE_FOR_PERFORMANCE = no;
          # CC_OPTIMIZE_FOR_PERFORMANCE_O3 = yes;

          # cpu opt
          GENERIC_CPU = no;
          MNATIVE = yes;

          # irq threading
          FORCE_IRQ_THREADING = no;

          # smt nice
          SMT_NICE = yes;

          # rq sharing
          RQ_NONE = no;
          RQ_SMT = no;
          RQ_MC = no;
          RQ_MC_LLC = yes;
          RQ_SMP = no;
          RQ_ALL = no;

          # timer freq
          HZ_300 = no;
          # HZ = 500;
          HZ_500 = yes;
          HZ_500_NODEF = yes;
          HZ_750 = no;
          HZ_750_NODEF = no;
          HZ_1000_NODEF = no;
          HZ_250_NODEF = no;
          HZ_300_NODEF = no;

          # default cpu gov
          # ondemand
          # CPU_FREQ_DEFAULT_GOV_SCHEDUTIL = no;
          # CPU_FREQ_DEFAULT_GOV_ONDEMAND = yes;
          # performance
          CPU_FREQ_DEFAULT_GOV_SCHEDUTIL = no;
          CPU_FREQ_DEFAULT_GOV_PERFORMANCE = yes;

        };

        extraConfig = ''
          ## Set some -tkg defaults
          NR_TTY_DEVICES 63

          CMDLINE intel_pstate=passive

          # timer freq
          HZ 500
        '';

        kernelPatches = [
          {
            name = "001";
            patch =
              ./tkg-kernel/0001-add-sysctl-to-disallow-unprivileged-CLONE_NEWUSER-by.patch;
          }
          {
            name = "002";
            patch = ./tkg-kernel/0002-clear-patches.patch;
          }
          {
            name = "003";
            patch = ./tkg-kernel/0003-glitched-base.patch;
          }
          # MuQSS
          # {
          #   # Apply default CPU sched yield type here
          #   name = "004";
          #   patch = ./tkg-kernel/0004-5.6-ck2.patch;
          # }
          # {
          #   name = "005";
          #   patch = ./tkg-kernel/0004-glitched-ondemand-muqss.patch;
          # }
          # {
          #   name = "006";
          #   patch = ./tkg-kernel/0004-glitched-muqss.patch;
          # }
          # PDS
          {
            # Apply default int sched_yield_type __read_mostly = 0; here
            name = "004";
            patch = ./tkg-kernel/0005-v5.6_undead-pds099o.patch;
          }
          {
            name = "005";
            patch = ./tkg-kernel/0005-glitched-ondemand-pds.patch;
          }
          {
            name = "006";
            patch = ./tkg-kernel/0005-glitched-pds.patch;
          }
          {
            name = "007";
            patch = ./tkg-kernel/0007-v5.6-fsync.patch;
          }
        ];

        allowImportFromDerivation = true;

        extraMeta.branch = "5.6";
      } // (args.argsOverride or { }));
    linux_tkg = pkgs.callPackage linux_tkg_pkg { };
  in pkgs.recurseIntoAttrs (pkgs.linuxPackagesFor linux_tkg);
}
