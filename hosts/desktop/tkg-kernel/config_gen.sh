#!/usr/bin/env bash

## Run when updating

# Set some -tkg defaults
echo "# CONFIG_DYNAMIC_FAULT is not set" >> ./.config
sed -i -e 's/CONFIG_DEFAULT_FQ_CODEL=y/# CONFIG_DEFAULT_FQ_CODEL is not set/' ./.config
echo "CONFIG_DEFAULT_CAKE=y" >> ./.config
echo "CONFIG_NR_TTY_DEVICES=63" >> ./.config
echo "CONFIG_TP_SMAPI=m" >> ./.config
echo "CONFIG_RAID6_USE_PREFER_GEN=y" >> ./.config
echo "# CONFIG_NTP_PPS is not set" >> ./.config
sed -i -e 's/CONFIG_CRYPTO_LZ4=m/CONFIG_CRYPTO_LZ4=y/' ./.config
sed -i -e 's/CONFIG_CRYPTO_LZ4HC=m/CONFIG_CRYPTO_LZ4HC=y/' ./.config
sed -i -e 's/CONFIG_LZ4_COMPRESS=m/CONFIG_LZ4_COMPRESS=y/' ./.config
sed -i -e 's/CONFIG_LZ4HC_COMPRESS=m/CONFIG_LZ4HC_COMPRESS=y/' ./.config
sed -i -e 's/CONFIG_RCU_BOOST_DELAY=500/CONFIG_RCU_BOOST_DELAY=0/' ./.config

# Inject cpuopts options
echo "# CONFIG_MK8SSE3 is not set" >> ./.config
echo "# CONFIG_MK10 is not set" >> ./.config
echo "# CONFIG_MBARCELONA is not set" >> ./.config
echo "# CONFIG_MBOBCAT is not set" >> ./.config
echo "# CONFIG_MJAGUAR is not set" >> ./.config
echo "# CONFIG_MBULLDOZER is not set" >> ./.config
echo "# CONFIG_MPILEDRIVER is not set" >> ./.config
echo "# CONFIG_MSTEAMROLLER is not set" >> ./.config
echo "# CONFIG_MEXCAVATOR is not set" >> ./.config
echo "# CONFIG_MZEN is not set" >> ./.config
echo "# CONFIG_MZEN2 is not set" >> ./.config
echo "# CONFIG_MATOM is not set" >> ./.config
echo "# CONFIG_MNEHALEM is not set" >> ./.config
echo "# CONFIG_MWESTMERE is not set" >> ./.config
echo "# CONFIG_MSILVERMONT is not set" >> ./.config
echo "# CONFIG_MSANDYBRIDGE is not set" >> ./.config
echo "# CONFIG_MIVYBRIDGE is not set" >> ./.config
echo "# CONFIG_MHASWELL is not set" >> ./.config
echo "# CONFIG_MBROADWELL is not set" >> ./.config
echo "# CONFIG_MSKYLAKE is not set" >> ./.config
echo "# CONFIG_MSKYLAKEX is not set" >> ./.config
echo "# CONFIG_MCANNONLAKE is not set" >> ./.config
echo "# CONFIG_MICELAKE is not set" >> ./.config

# MuQSS default config
echo "CONFIG_SCHED_MUQSS=y" >> ./.config
# Disable CFS
sed -i -e 's/CONFIG_FAIR_GROUP_SCHED=y/# CONFIG_FAIR_GROUP_SCHED is not set/' ./.config
sed -i -e 's/CONFIG_CFS_BANDWIDTH=y/# CONFIG_CFS_BANDWIDTH is not set/' ./.config
sed -i -e 's/CONFIG_CGROUP_CPUACCT=y/# CONFIG_CGROUP_CPUACCT is not set/' ./.config
sed -i -e 's/CONFIG_SCHED_AUTOGROUP=y/# CONFIG_SCHED_AUTOGROUP is not set/' ./.config

# zenify
echo "CONFIG_ZENIFY=y" >> ./.config

# compiler optimization level
sed -i -e 's/CONFIG_CC_OPTIMIZE_FOR_PERFORMANCE=y/# CONFIG_CC_OPTIMIZE_FOR_PERFORMANCE is not set/' ./.config
echo "CONFIG_CC_OPTIMIZE_HARDER=y" >> ./.config

# cpu opt
echo "CONFIG_MNATIVE=y" >> ./.config

# irq threading
echo "# CONFIG_FORCE_IRQ_THREADING is not set" >> ./.config

# smt nice
echo "CONFIG_SMT_NICE=y" >> ./.config

# rq sharing
echo -e "# CONFIG_RQ_NONE is not set\n# CONFIG_RQ_SMT is not set\nCONFIG_RQ_MC=y\n# CONFIG_RQ_MC_LLC is not set\n# CONFIG_RQ_SMP is not set\n# CONFIG_RQ_ALL is not set" >> ./.config

# timer freq
sed -i -e 's/CONFIG_HZ_300=y/# CONFIG_HZ_300 is not set/' ./.config
sed -i -e 's/CONFIG_HZ_300_NODEF=y/# CONFIG_HZ_300_NODEF is not set/' ./.config
sed -i -e 's/CONFIG_HZ=300/CONFIG_HZ=500/' ./.config
echo "CONFIG_HZ_500=y" >> ./.config
echo "CONFIG_HZ_500_NODEF=y" >> ./.config
echo "# CONFIG_HZ_750 is not set" >> ./.config
echo "# CONFIG_HZ_750_NODEF is not set" >> ./.config
echo "# CONFIG_HZ_1000_NODEF is not set" >> ./.config
echo "# CONFIG_HZ_250_NODEF is not set" >> ./.config
echo "# CONFIG_HZ_300_NODEF is not set" >> ./.config
