{ config, options, lib, pkgs, ... }:
with lib;
let
  sddmAvatarFor = username: icon: pkgs.runCommand "sddm-avatar-${username}"
    { inherit icon username; }
    ''
      mkdir -p "$out/share/sddm/faces/"
      cp "$icon" "$out/share/sddm/faces/$username.face.icon"
    '';
in
{
  environment.systemPackages = [
    (sddmAvatarFor config.my.username <modules/desktop/plasma/avatars/Blackbox.png>)
  ];
  # SDDM/KDE Avatar
  my.home.home.file = {
    ".face.icon".source = <modules/desktop/plasma/avatars/Blackbox.png>;
  };
}
