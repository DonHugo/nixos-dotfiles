# hosts/personal.nix --- common settings

{ config, lib, pkgs, ... }:
{
  # Nothing in /tmp should survive a reboot
  boot.tmpOnTmpfs = true;
  # Use systemd bootloader
  boot = {
    loader.systemd-boot = {
      enable = true;
      # Limit number of generations to display in boot menu
      configurationLimit = 10;
    };
    loader.efi.canTouchEfiVariables = true;
    kernelParams = [ "quiet" "udev.log_priority=3" ];
    loader.timeout = 1;
  };

  # Location
  location.provider = "geoclue2";

  # Obey XDG conventions;
  my.home.xdg.enable = true;
  environment.variables = {
    # These are the defaults, but some applications are buggy when these lack
    # explicit values.
    XDG_CONFIG_HOME = "$HOME/.config";
    XDG_CACHE_HOME  = "$HOME/.cache";
    XDG_DATA_HOME   = "$HOME/.local/share";
    XDG_BIN_HOME    = "$HOME/.local/bin";
  };

  # Clean up leftovers, as much as we can
  system.activationScripts.clearHome = ''
    pushd /home/${config.my.username}
    rm -rf .compose-cache .nv .pki .dbus .fehbg
    [ -s .xsession-errors ] || rm -f .xsession-errors*
    popd
  '';
}
