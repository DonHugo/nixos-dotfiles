# Work laptop

{ config, lib, pkgs, ... }:

let
  custompkgs = import <custompkgs> {};
in
{

  imports = [
    # System specific configuration
    ./hardware-configuration.nix
    # Common settings
    ../personal.nix
    # Dekstop environment
    <modules/desktop/plasma>
    # Apps
    <modules/network/nextcloud.nix>
    <modules/network/samba.nix>
    <modules/network/rdp.nix>
    <modules/network/riot.nix>
    <modules/network/zoom.nix>
    <modules/browser/vivaldi.nix>
    <modules/office/libreoffice.nix>
    <modules/editors/emacs.nix>
    # Shell
    <modules/shell/fish.nix>
    # Hardware
    <modules/hardware/hpPrinter.nix>
    # Fonts
    <modules/fonts>
    # Misc
    <modules/misc/keepassxc.nix>
    <modules/misc/virtualbox.nix>
    <modules/misc/arch-install-scripts.nix>
  ];


  # Select internationalisation properties.
  i18n = {
    consoleFont = "Lat2-Terminus16";
    consoleKeyMap = "us";
    defaultLocale = "de_DE.UTF-8";
  };
  # Keyboard and touchpad
  services.xserver = {
    layout = "eu";
    libinput.enable = true;
    libinput.naturalScrolling = true;
    libinput.disableWhileTyping = true;
  };

  # Time zone.
  time.timeZone = "Europe/Zurich";

  # Power users
  # TLP
  powerManagement.cpuFreqGovernor =
    lib.mkIf config.services.tlp.enable (lib.mkForce null);
  services.tlp.enable = true;

  # Flatpak
  services.flatpak.enable = true;
  xdg.portal.enable = true;
}
