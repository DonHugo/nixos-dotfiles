# Machine specific stettings

{ config, lib, pkgs, ... }:

{
  # Packages
  environment.systemPackages = with pkgs; [
    lm_sensors
  ];

  # Hardware and system settings
  hardware.enableRedistributableFirmware = true;

  # CPU
  # Maximum number of parallel build jobs
  nix.maxJobs = lib.mkDefault 8;
  # Default CPU governor
  powerManagement.cpuFreqGovernor = lib.mkDefault "powersave";
  hardware.cpu.intel.updateMicrocode = true;
  services.thermald.enable = true;

  # SSD
  # Fstrim
  services.fstrim.enable = true;
  # Mounts
  fileSystems."/".options = [ "noatime" ];
  # LUKS disk encryption
  boot.initrd.luks.devices = [
    {
      name = "root";
      device = "dev/nvme0n1p2";
      preLVM = true;
    }
  ];

  # GPU
  boot.initrd.kernelModules = [ "i915" ];
  services.xserver.videoDrivers = [ "intel" ];
  # OpenGL
  hardware.opengl = {
    enable = true;
    extraPackages = with pkgs; [
      vaapiIntel
      vaapiVdpau
      libvdpau-va-gl
    ];
  };

  # Networking
  # Wifi
  networking.networkmanager.enable = true;
  # The global useDHCP flag is deprecated, therefore explicitly set to false
  # here. Per-interface useDHCP will be mandatory in the future, so this
  # generated config replicates the default behaviour.
  networking.useDHCP = false;
  networking.interfaces.enp0s31f6.useDHCP = true;
  networking.interfaces.wlp2s0.useDHCP = true;

  # Monitor backlight control
  programs.light.enable = true;
  hardware.acpilight.enable = true;
}
