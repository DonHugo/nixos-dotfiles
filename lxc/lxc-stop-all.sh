#!/usr/bin/env bash

# Especially useful to shutdown properly all containers before rebooting the LXC host.

RUNNING_CONTAINERS="$(lxc ls --format csv | cut -f1 -d",")"

for container in $RUNNING_CONTAINERS; do
  echo "Stopping $container..."
  lxc stop $container
done
