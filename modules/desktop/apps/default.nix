{ config, lib, pkgs, ... }:

{
  imports = [
    ./keepass.nix
    ./nextcloud.nix
    ./redshift.nix
    ./riot.nix
    ./zoom.nix
  ];
}
