{ config, options, lib, pkgs, ... }:

with lib;

{
  options.modules.desktop.apps.keepass = {
    enable = mkOption {
      type = types.bool;
      default = false;
    };
  };

  config = mkIf config.modules.desktop.apps.keepass.enable {
    environment.systemPackages = with pkgs; [
      keepassxc
    ];
  };
}
