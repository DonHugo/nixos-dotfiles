{ config, options, lib, pkgs, ... }:

with lib;

{
  options.modules.desktop.apps.nextcloud = {
    enable = mkOption {
      type = types.bool;
      default = false;
    };
  };

  config = mkIf config.modules.desktop.apps.nextcloud.enable {
    environment.systemPackages = with pkgs; [
      nextcloud-client
    ];
  };
}
