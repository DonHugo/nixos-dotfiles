{ config, options, lib, pkgs, ... }:

with lib;

{
  options.modules.desktop.apps.redshift = {
    enable = mkOption {
      type = types.bool;
      default = false;
    };
  };

  config = mkIf config.modules.desktop.apps.redshift.enable {
    services.redshift.enable = true;
  };
}
