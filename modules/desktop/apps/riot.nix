{ config, options, lib, pkgs, ... }:

with lib;

{
  options.modules.desktop.apps.riot = {
    enable = mkOption {
      type = types.bool;
      default = false;
    };
  };

  config = mkIf config.modules.desktop.apps.riot.enable {
    environment.systemPackages = with pkgs; [
      unstable.riot-desktop
    ];
  };
}
