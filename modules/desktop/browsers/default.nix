{ config, lib, pkgs, ... }:

with lib;

{
  imports = [
    ./vivaldi.nix
  ];

  options.modules.desktop.browsers = {
    default = mkOption {
      type = with types; nullOr str;
      default = null;
    };
  };
}
