{ config, options, lib, pkgs, ... }:

with lib;

let
  unstable = import <nixos-unstable> { config.allowUnfree = true; };
  custompkgs = import <custompkgs> { };
in {
  options.modules.desktop.browsers.vivaldi = {
    enable = mkOption {
      type = types.bool;
      default = false;
    };
  };

  config = mkIf config.modules.desktop.browsers.vivaldi.enable {

    environment.systemPackages = with pkgs;
      [
        (custompkgs.vivaldi.overrideAttrs (oldAttrs: rec {
          buildPhase = ''
            ${oldAttrs.buildPhase}
            ln -s ${custompkgs.vivaldi-ffmpeg-codecs}/lib/libffmpeg.so opt/vivaldi/libffmpeg.so.''${version%\.*\.*}
          '';
          installPhase = ''
            ${oldAttrs.installPhase}
            mkdir -p $out/opt/google/chrome/
            ln -sf ${custompkgs.vivaldi-widevine}/share/google/chrome/WidevineCdm $out/opt/google/chrome/WidevineCdm
            ln -sf ${custompkgs.vivaldi-widevine}/share/google/chrome/WidevineCdm $out/opt/vivaldi/WidevineCdm '';
        }))
      ];
  };
}
