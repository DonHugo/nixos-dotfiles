{ config, lib, pkgs, ... }:

{
  imports = [
    ./gnome
    ./plasma

    ./apps
    ./browsers
    ./gaming
    ./media
    ./office
  ];
}
