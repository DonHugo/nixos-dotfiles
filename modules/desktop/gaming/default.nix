{ config, options, lib, pkgs, ... }:

with lib;
let
  custompkgs = import <custompkgs> { };
  unstable = import <nixos-unstable> {
    config.allowUnfree = true;
    # For Lutris
    config.permittedInsecurePackages = [ "p7zip-16.02" ];
  };
in {
  options.modules.desktop.gaming = {
    enable = mkOption {
      type = types.bool;
      default = false;
    };
  };

  config = mkIf config.modules.desktop.gaming.enable {

    environment.systemPackages = with pkgs; [
      glxinfo
      unstable.steam
      unstable.lutris
      wine-staging
    ];

    # hardware.opengl.package = pkgs.unstable.mesa;
    hardware.opengl.driSupport32Bit = true;
    hardware.pulseaudio.support32Bit = true;
  };
}
