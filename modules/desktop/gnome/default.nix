{ config, options, lib, pkgs, ... }:
with lib;

let custompkgs = import <custompkgs> { };
in {
  imports = [ ./theme.nix ];

  options.modules.desktop.gnome = {
    enable = mkOption {
      type = types.bool;
      default = false;
    };
  };

  config = mkIf config.modules.desktop.gnome.enable {

    # Basic configuration
    services.xserver = {
      enable = true;
      desktopManager.gnome3.enable = true;
    };

    # GDM configuration
    services.xserver = { displayManager = { gdm = { enable = true; }; }; };

    # Gome keyring
    services.gnome3.gnome-keyring.enable = true;
    security.pam.services.gdm.enableGnomeKeyring = true;
    # For Gnome software
    programs.dconf.enable = true;

    # Audio
    sound.enable = true;
    hardware.pulseaudio = {
      enable = true;
      support32Bit = true;
      extraModules = [ pkgs.pulseaudio-modules-bt ];
      package = pkgs.pulseaudioFull;
    };

    # Bluetooth
    hardware.bluetooth = {
      enable = true;
      package = pkgs.bluezFull;
    };

    # Additional packages
    environment.systemPackages = with pkgs; [
      gnome3.gnome-tweaks
      gnomeExtensions.gsconnect
      gnomeExtensions.dash-to-panel
      gnomeExtensions.appindicator
    ];

    # Config
    # my.home.home.file = { ".xinitrc".source = ./xinitrc; };
    my.home.home.file = {
      ".profile".text = ''
        export QT_QPA_PLATFORMTHEME="qt5ct"
      '';
    };
  };
}
