{ config, options, lib, pkgs, ... }:

with lib;

{
  options.modules.desktop.media.clementine = {
    enable = mkOption {
      type = types.bool;
      default = false;
    };
  };

  config = mkIf config.modules.desktop.media.clementine.enable {
    # Packages
    environment.systemPackages = with pkgs; [
      clementine
      clementineUnfree
    ];
  };
}
