{ config, lib, pkgs, ... }:

{
  imports = [
    ./clementine.nix
    ./vlc.nix
  ];
}
