{ config, options, lib, pkgs, ... }:

with lib;

{
  options.modules.desktop.media.vlc = {
    enable = mkOption {
      type = types.bool;
      default = false;
    };
  };

  config = mkIf config.modules.desktop.media.vlc.enable {
    # Packages
    environment.systemPackages = with pkgs; [
      libsForQt5.vlc
    ];
  };
}
