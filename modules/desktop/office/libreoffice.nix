{ config, options, lib, pkgs, ... }:

with lib;

let
  custompkgs = import <custompkgs> {};
in
{
  options.modules.desktop.office.libreoffice = {
    enable = mkOption {
      type = types.bool;
      default = false;
    };
  };

  config = mkIf config.modules.desktop.office.libreoffice.enable {
  # Packages
  environment.systemPackages = with pkgs; [
    libreoffice-fresh
    hunspell
    hunspellDicts.de_DE
    hunspellDicts.en_US-large
    mythes
    custompkgs.mythes-de
    custompkgs.mythes-en
    hyphen
    custompkgs.hyphen-de
  ];

  # Make links available
  environment.pathsToLink = [ "/share/hunspell"
                              "/share/mythes"
                              "/share/hyphen" ];

  # Export links into environment
  services.xserver.displayManager.sessionCommands = ''
  for PROFILE in $NIX_PROFILES; do
    HDIR="$PROFILE/share/mythes"
    if [ -d "$HDIR" ]; then
        export DICPATH=$DICPATH''${DICPATH:+:}$HDIR
    fi
  done
  for PROFILE in $NIX_PROFILES; do
    HDIR="$PROFILE/share/hyphen"
    if [ -d "$HDIR" ]; then
        export DICPATH=$DICPATH''${DICPATH:+:}$HDIR
    fi
  done
  '';
  };
}
