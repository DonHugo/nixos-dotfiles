{ config, options, lib, pkgs, ... }:
with lib;

let
  custompkgs = import <custompkgs> {};
in
{
  imports = [
    ./theme.nix
  ];

  options.modules.desktop.plasma = {
    enable = mkOption { type = types.bool; default = false; };
  };

  config = mkIf config.modules.desktop.plasma.enable {

    # Basic configuration
    services.xserver = {
      enable = true;
      desktopManager.plasma5.enable = true;
    };

    # SDDM configuration
    services.xserver = {
      displayManager.sddm = {
        enable = true;
        theme = "breeze";
        extraConfig = ''
          [Theme]
          CursorTheme=Breeze_Snow
          '';
      };
    };

    # Gome keyring
    services.gnome3.gnome-keyring.enable = true;
    security.pam.services.sddm.enableGnomeKeyring = true;
    # For Gnome software
    programs.dconf.enable = true;

    # Audio
    sound.enable = true;
    hardware.pulseaudio = {
      enable = true;
      support32Bit = true;
      extraModules = [ pkgs.pulseaudio-modules-bt ];
      package = pkgs.pulseaudioFull;
    };

    # Bluetooth
    hardware.bluetooth = {
      enable = true;
      package = pkgs.bluezFull;
    };

    # Additional packages
    environment.systemPackages = with pkgs; [
      accountsservice
      ark
      gwenview
      libsForQt5.kimageformats
      qt5.qtimageformats
      kate
      kcalc
      kdeApplications.kdenetwork-filesharing
      kdeApplications.spectacle
      custompkgs.kdeconnect
      kdeFrameworks.kio
      custompkgs.kshutdown
      latte-dock
      custompkgs.latte-applet-spacer
      custompkgs.latte-applet-separator
      okular
      plasma5.sddm-kcm
      plasma5.user-manager
      skanlite
      gnome3.dconf
      sound-theme-freedesktop
      (redshift-plasma-applet.overrideAttrs (oldAttrs: rec {

        src = fetchFromGitHub {
          owner = "KDE";
          repo = "plasma-redshift-control";
          rev = "2476d6e883c3f72b54b43adb8a5302814e2f1b6a";
          sha256 = "13k28pci8bk2gvmd0fz6xzv8cj8dwji9kiqkczqz6ybs6qxcgyjn";
        };
      })
      )
    ];
  };
}
