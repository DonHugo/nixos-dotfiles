{ config, lib, pkgs, ... }:

let
  custompkgs = import <custompkgs> {};
in
{
  environment.systemPackages = with pkgs; [
    (libsForQt5.qtstyleplugin-kvantum.overrideAttrs (oldAttrs: rec {
      version = "0.15.2";

      src = fetchFromGitHub {
        owner = "tsujan";
        repo = "Kvantum";
        rev = "V${version}";
        sha256 = "0cv0lxyi2sr0k7f03rsh1j28avdxd0l0480jsa95ca3d2lq392g3";
      };
     
      postInstall = ''
      # Let Kvantummanager find the themes specified below. This needs to get
      # adjusted every time a theme is added
      # find ${pkgs.arc-kde-theme}/share/Kvantum/ -maxdepth 1 -mindepth 1 -type d -exec ln -sv '{}' $out/share/Kvantum/ \;
      # find ${pkgs.adapta-kde-theme}/share/Kvantum/ -maxdepth 1 -mindepth 1 -type d -exec ln -sv '{}' $out/share/Kvantum/ \;
      find ${custompkgs.kvantum-theme-materia}/share/Kvantum/ -maxdepth 1 -mindepth 1 -type d -exec ln -sv '{}' $out/share/Kvantum/ \;
      '';
    })
    )
    custompkgs.materia-kde-theme
    (materia-theme.overrideAttrs (oldAttrs: rec {
      pname = "materia-theme";
      version = "20200320";

      src = fetchFromGitHub {
        owner = "nana-4";
        repo = pname;
        rev = "v${version}";
        sha256 = "0g4b7363hzs7z9xghldlz9aakmzzp18hhx32frb6qxy04lna2lwk";
      };
    }))
    papirus-icon-theme
    gnome3.adwaita-icon-theme
    custompkgs.solus-wallpaper
  ];
}
