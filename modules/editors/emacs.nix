{ config, options, lib, pkgs, ... }:

with lib;

let custompkgs = import <custompkgs> { };
in {
  options.modules.editors.emacs = {
    enable = mkOption {
      type = types.bool;
      default = false;
    };
  };

  config = mkIf config.modules.editors.emacs.enable {
    environment.systemPackages = with pkgs; [
      emacs
      git
      (ripgrep.override { withPCRE2 = true; })
      # Optional dependencies
      # faster projectile indexing
      fd
      # for TLS connectivity
      gnutls
      # for image-dired
      imagemagick
      # in-emacs gnupg prompts
      (lib.mkIf (config.programs.gnupg.agent.enable) pinentry_emacs)
      # for undo-tree compression
      zstd

      ## Module dependencies
      # :ui treemacs
      python3
      # :checkers spell
      aspell
      aspellDicts.en
      aspellDicts.en-computers
      aspellDicts.en-science
      aspellDicts.de
      # :lang latex & :lang org (latex previews)
      (texlive.combine {
        inherit (texlive) scheme-medium
          # moderncv
          fontawesome;
      })
      (makeDesktopItem {
        name = "emacs-client";
        desktopName = "Emacs Client";
        genericName = "Text Editor";
        icon = "emacs";
        exec = "${emacs}/bin/emacsclient -c -a emacs";
        categories = "Development;TextEditor;";
      })
      # :lang markdown & :tools pandoc
      unstable.haskellPackages.pandoc
      unstable.haskellPackages.pandoc-citeproc
      unstable.haskellPackages.pandoc-crossref
    ];

    my.env.PATH = [ "$HOME/.emacs.d/bin" ];

    fonts.fonts =
      [ pkgs.emacs-all-the-icons-fonts custompkgs.ttf-iosevka-ss08 ];
  };
}
