{ config, options, lib, pkgs, ... }:
with lib;

let
  custompkgs = import <custompkgs> {};
in
{
  imports = [
    ./options.nix
  ];

  options.modules.fonts = {
    enable = mkOption { type = types.bool; default = false; };
  };

  config = mkIf config.modules.fonts.enable {

    fonts = {
      enableFontDir = true;
      enableGhostscriptFonts = true;
      fonts = with pkgs; [
        # For meta package
        caladea
        cantarell-fonts
        carlito
        custompkgs.ttf-clear-sans
        custompkgs.ttf-courier-prime
        dejavu_fonts
        custompkgs.ttf-droid
        gelasio
        custompkgs.ttf-gsfonts
        gyre-fonts
        custompkgs.ttf-heuristica
        custompkgs.ttf-impallari-cantora
        liberation_ttf
        custompkgs.ttf-merriweather
        custompkgs.ttf-merriweather-sans
        noto-fonts
        open-sans
        custompkgs.ttf-oswald
        custompkgs.ttf-quintessential
        custompkgs.ttf-signika
        symbola
        ubuntu_font_family
        xorg.fontmiscmisc
        # Meta package
        custompkgs.fonts-meta
        # Other
        hack-font
        roboto-slab
        # Microsoft
        corefonts
        vistafonts
      ];

      fontconfig.defaultFonts = {
        sansSerif = [ "Noto Sans" ];
        serif = [ "Roboto Slab" ];
        monospace = [ "Hack" ];
      };

      fontconfig.subpixel.lcdfilter = "light";
      fontconfig.meta.enable = true;
    };

    # https://wiki.archlinux.org/index.php/java#Better_font_rendering
    services.xserver.displayManager.sessionCommands = ''
    export _JAVA_OPTIONS='-Dawt.useSystemAAFontSettings=on -Dswing.aatext=true'
    # Subpixel hinting mode can be chosen by setting the right TrueType interpreter
    # version. The available settings are:
    #
    #     truetype:interpreter-version=35  # Classic mode (default in 2.6)
    #     truetype:interpreter-version=38  # Infinality mode
    #     truetype:interpreter-version=40  # Minimal mode (default in 2.7)
    #
    # There are more properties that can be set, separated by whitespace. Please
    # refer to the FreeType documentation for details.

    # Uncomment and configure below
    export FREETYPE_PROPERTIES="truetype:interpreter-version=38"
    '';
  };
}
