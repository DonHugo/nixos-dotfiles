{ config, pkgs, lib, ... }:


with lib;

let cfg = config.fonts.fontconfig.meta;

    custompkgs = import <custompkgs> {};

    # The configuration to be included in /etc/font/
    confPkg = pkgs.runCommand "font-meta-conf" { preferLocalBuild = true; } ''
      support_folder=$out/etc/fonts/conf.d

      mkdir -p $support_folder

      # fonts-meta various configuration files
      ln -s ${custompkgs.fonts-meta}/etc/fonts/conf.d/*.conf \
            $support_folder
      ln -s ${custompkgs.ttf-droid}/etc/fonts/conf.d/*.conf \
            $support_folder
      ln -s ${custompkgs.ttf-gsfonts}/etc/fonts/conf.d/*.conf \
            $support_folder
    '';

in
{

  options = {

    fonts = {

      fontconfig = {

        meta = {
          enable = mkOption {
            type = types.bool;
            default = true;
            description = ''
              Enable Arch's fonts-meta-extended fontconfig settings (formerly known as
              Infinality).
            '';
          };
        };
      };
    };

  };

  config = mkIf (config.fonts.fontconfig.enable && cfg.enable) {

    fonts.fontconfig.confPackages = [ confPkg ];
  };

}
