{ config, options, lib, pkgs, ... }:

with lib;

let
  custompkgs = import <custompkgs> { };
  broadcom = pkgs.fetchFromGitHub {
    owner = "winterheart";
    repo = "broadcom-bt-firmware";
    rev = "v12.0.1.1105";
    sha256 = "0sxk1d4idpx4im7azy5983hfqiqikps11z89f2q0zysf6cjmcgh9";
  };
in {
  options.modules.hardware.broadcom-bt-firmware = {
    enable = mkOption {
      type = types.bool;
      default = false;
    };
  };

  config = mkIf config.modules.hardware.broadcom-bt-firmware.enable {
    hardware.firmware = [
      (pkgs.runCommandNoCC "firmware-broadcom-bt" { } ''
        mkdir -p $out/lib/firmware/brcm
        cp ${broadcom}/brcm/*.hcd $out/lib/firmware/brcm
      '')
    ];
  };
}
