{ config, lib, pkgs, ... }:

{
  imports = [
    ./broadcom-bt-firmware.nix
    ./hpPrinter.nix
    ./xpadneo.nix
  ];
}
