{ config, options, lib, pkgs, ... }:

with lib;

let
  custompkgs = import <custompkgs> {};
in
{
  options.modules.hardware.hpPrinter = {
    enable = mkOption {
      type = types.bool;
      default = false;
    };
  };

  config = mkIf config.modules.hardware.hpPrinter.enable {
    services.printing = {
      enable = true;
      drivers = with pkgs; [
        custompkgs.hplipWithPlugin
      ];
    };

    hardware.sane = {
      enable = true;
      extraBackends = [
        custompkgs.hplipWithPlugin
      ];
    };

    my.user = {
      extraGroups = [ "lp" "scanner" ];
    };
  };
}
