{ config, options, lib, pkgs, ... }:

with lib;

let
  custompkgs = import <custompkgs> {};
in
{
  options.modules.hardware.xpadneo = {
    enable = mkOption {
      type = types.bool;
      default = false;
    };
  };

  config = mkIf config.modules.hardware.xpadneo.enable {

    # XBOX gamepad
    boot.extraModulePackages = [ config.boot.kernelPackages.xpadneo ];
    services.udev.packages = [ config.boot.kernelPackages.xpadneo ];
    boot.kernelModules = [ "hid-xpadneo" ];
    boot.extraModprobeConfig = ''
    options bluetooth disable_ertm=y
    '';

  };
}
