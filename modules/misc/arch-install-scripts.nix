{ config, lib, pkgs, ... }:

let
  custompkgs = import <custompkgs> {};
in
{
  environment.systemPackages = with pkgs; [
    custompkgs.arch-install-scripts
    pacman
  ];
}
