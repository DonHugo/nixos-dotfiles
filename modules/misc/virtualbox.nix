{ config, lib, pkgs, ... }:

let
  custompkgs = import <custompkgs> {};
in
{
  virtualisation.virtualbox = {
    host.enable = true;
    host.enableExtensionPack = true;
  };
  users.extraGroups.vboxusers.members = [ "roland" ];
}
