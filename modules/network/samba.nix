{ config, lib, pkgs, ... }:

let
  custompkgs = import <custompkgs> {};
in
{
  services.gvfs.enable = true;
}
