{ config, lib, pkgs, ... }:

let
  custompkgs = import <custompkgs> {};

in
{
  imports = [
    ./rofi.nix
    ./redshift.nix
  ];

  # Packages
  environment.systemPackages = with pkgs; [
    # Display manager
    lightdm
    # Notificaitons
    dunst
    libnotify
    # Polybar
    (polybar.override {
      pulseSupport = true;
      nlSupport = true;
    })
    # tabbed windows
    tabbed
    xdotool
    xorg.xwininfo
    # PDF reader
    evince
    # Wallpaper
    nitrogen
    # X clipboard
    xclip
    clipit
    # File manager
    # (xfce.thunar.override {
    #   # Thunar plugin providing file context menus for archives
    #   thunarPlugins = [ xfce.thunar-archive-plugin
    #                     # Thunar extension for automatic management of removable
    #                     # drives and media
    #                     xfce.thunar-volman ];})
    # Thumbnails for thunar
    # xfce.tumbler
    # Archives
    gnome3.file-roller
    openconnect_pa
    # Taskmanager
    xfce.xfce4-taskmanager
    # Terminal
    gnome3.gnome-terminal
    # Manage application themes
    qt5ct
    libsForQt5.qtstyleplugins
    # Monitor management
    arandr
    autorandr
    # Rofi launcher
    rofi
    my.rofi-filemenu
    # Lockscreen
    i3lock
    imagemagick
    unstable.xidlehook
    # Screenshots
    scrot
    # Sound
    pavucontrol
  ];

  # For Gnome software
  programs.dconf.enable = true;

  # Display
  services = {
    # X11
    xserver = {
      enable = true;
      desktopManager.xterm.enable = lib.mkDefault false;
      desktopManager = {
        default = "xfce";
        xfce = {
          enable = true;
          noDesktop = true;
          enableXfwm = false;
        };
      };
      windowManager.default = "bspwm";
      windowManager.bspwm.enable = true;
      displayManager.lightdm.enable = true;
      displayManager.lightdm.background = "${custompkgs.solus-wallpaper}/share/backgrounds/solus/LakeSideView.jpg";
      displayManager.lightdm.greeters.gtk = {
        enable = true;
        iconTheme.name = "Papirus";
        iconTheme.package = pkgs.papirus-icon-theme;
        theme.name = "Arc";
        theme.package = pkgs.arc-theme;
        cursorTheme.package = custompkgs.xcursor-breeze;
        cursorTheme.name = "Breeze";
      };
    };
    # Compton
    compton = {
      enable = true;
      backend = "glx";
      vSync = true;
      fade = true;
      fadeDelta = 1;
      fadeSteps = [ "0.01" "0.012" ];
      shadow = true;
      shadowOffsets = [ (-10) (-10) ];
      shadowOpacity = "0.22";
      # activeOpacity = "1.00";
      # inactiveOpacity = "0.92";
      settings = {
        shadow-radius = 12;
        # blur-background = true;
        # blur-background-frame = true;
        # blur-background-fixed = true;
        blur-kern = "7x7box";
        blur-strength = 320;
      };
      opacityRules = [
        # "100:class_g = 'Firefox'"
        # "100:class_g = 'Vivaldi-stable'"
        "100:class_g = 'VirtualBox Machine'"
        # Art/image programs where we need fidelity
        "100:class_g = 'Gimp'"
        "100:class_g = 'Inkscape'"
        "100:class_g = 'aseprite'"
        "100:class_g = 'krita'"
        "100:class_g = 'feh'"
        "100:class_g = 'mpv'"
        "100:class_g = 'Rofi'"
        "100:class_g = 'Peek'"
        "99:_NET_WM_STATE@:32a = '_NET_WM_STATE_FULLSCREEN'"
      ];
      # shadowExclude = [
      #   # Put shadows on notifications, the scratch popup and rofi only
      #   "! name~='(scratch|Dunst)$'"
      # ];
      settings.blur-background-exclude = [
        "window_type = 'dock'"
        "window_type = 'desktop'"
        "class_g = 'Rofi'"
        "_GTK_FRAME_EXTENTS@:c"
      ];
    };
  };

  # Security
  security.pam.services.lightdm.enableGnomeKeyring = true;
  programs.seahorse.enable = true;
  services.gnome3.gnome-keyring.enable = true;

  # Sound + Bluetooth
  sound.enable = true;
  hardware.bluetooth.enable = true;
  services.blueman.enable = true;
  hardware.pulseaudio = {
    enable = true;
    support32Bit = true;
    extraModules = [ pkgs.pulseaudio-modules-bt ];
    package = pkgs.pulseaudioFull;
  };
  ## Fonts
  fonts = {
    fonts = with pkgs; [
      font-awesome-ttf
      font-awesome_4
    ];
  };

  # Try really hard to get QT to respect my GTK theme.
  my.env.GTK_DATA_PREFIX = [ "${config.system.path}" ];
  my.env.QT_QPA_PLATFORMTHEME = "qt5ct";
  qt5 = { style = "gtk2"; platformTheme = "gtk2"; };
  services.xserver.displayManager.sessionCommands = ''
    # export GTK2_RC_FILES="$XDG_CONFIG_HOME/gtk-2.0/gtkrc"
    source "$XDG_CONFIG_HOME"/xsession/*.sh
    xrdb -merge "$XDG_CONFIG_HOME"/xtheme/*
  '';

  my.home.xdg.configFile = {
    # GTK
    "gtk-3.0/settings.ini".text = ''
      [Settings]
      gtk-theme-name=Arc-Darker
      gtk-icon-theme-name=Papirus
      gtk-fallback-icon-theme=gnome
      gtk-application-prefer-dark-theme=false
      gtk-cursor-theme-name=Breeze
      gtk-xft-antialias=1
      gtk-xft-hinting=1
      gtk-xft-hintstyle=hintslight
      gtk-xft-rgba=rgb
    '';
    # # QT4/5 global theme
    # "Trolltech.conf".text = ''
    #   [Qt]
    #   style=Ant-Dracula
    # '';
  };
  my.home.home.file = {
    # GTK2 global theme (widget and icon theme)
    ".gtkrc-2.0".text = ''
      gtk-theme-name="Arc-Darker"
      gtk-icon-theme-name="Papirus"
      gtk-font-name="Noto Sans 10"
      gtk-cursor-theme-name="Breeze"
      gtk-xft-antialias=1
      gtk-xft-hinting=1
      gtk-xft-hintstyle="hintslight"
      gtk-xft-rgba="rgb"
    '';
  };

 systemd.user.services.i3lock = {
   enable = true;
   before = [ "sleep.target"
              "hibernate.target"
              "suspend.target" ];
   wantedBy = [ "sleep.target"
                "hibernate.target"
                "suspend.target" ];
   description = "Lock screen when going to sleep/suspend/hibernate";
   path = [ pkgs.imagemagick
            pkgs.i3lock ];
   script = ''
     # take screenshot
     import -window root /tmp/screenshot.png

     # blur it
     convert /tmp/screenshot.png -blur 0x5 /tmp/screenshotblur.png
     rm /tmp/screenshot.png

     # lock the screen
     i3lock -i /tmp/screenshotblur.png

     # sleep 1 adds a small delay to prevent possible race conditions with suspend
     sleep 1

     exit 0
   '';
   serviceConfig = {
     User = "roland";
     Type = "simple";
     Environment = "DISPLAY=:0";
     TimeoutSec = "infinity";
   };
 };
}
