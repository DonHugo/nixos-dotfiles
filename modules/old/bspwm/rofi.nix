{ config, lib, pkgs, ... }:

{
  my = {

    packages = with pkgs; [

      # Fake rofi dmenu entries
      (makeDesktopItem {
        name = "reboot";
        desktopName = "System: Reboot";
        icon = "system-reboot";
        exec = "${<bin/i3exit>} reboot";
      })
      (makeDesktopItem {
        name = "shutdown";
        desktopName = "System: Shut Down";
        icon = "system-shutdown";
        exec = "${<bin/i3exit>} shutdown";
      })
      (makeDesktopItem {
        name = "sleep";
        desktopName = "System: Sleep";
        icon = "system-suspend";
        exec = "${<bin/i3exit>} suspend";
      })
      (makeDesktopItem {
        name = "lock-display";
        desktopName = "Lock screen";
        icon = "system-lock-screen";
        exec = "${<bin/i3exit>} lock";
      })
    ];
  };
}
