{ config, lib, pkgs, ... }:

let
  custompkgs = import <custompkgs> {};

in
{
  # imports = [
  #   ./theme.nix
  # ];

  # Packages
  environment.systemPackages = with pkgs; [
    # Display manager
    lightdm
    # Notificaitons
    dunst
    libnotify
    # Polybar
    (polybar.override {
      pulseSupport = true;
      nlSupport = true;
    })
    # PDF reader
    evince
    # Wallpaper
    nitrogen
    # X clipboard
    xclip
    clipit
    # File manager
    (xfce.thunar.override {
      # Thunar plugin providing file context menus for archives
      thunarPlugins = [ xfce.thunar-archive-plugin
                        # Thunar extension for automatic management of removable
                        # drives and media
                        xfce.thunar-volman ];})
    # Thumbnails for thunar
    xfce.tumbler
    # Archives
    gnome3.file-roller
    # Task manager
    xfce.xfce4-taskmanager
    # Networkmanager applet
    networkmanagerapplet
    # Terminal
    gnome3.gnome-terminal
    # Manage application themes
    lxappearance
    qt5ct
    libsForQt5.qtstyleplugins
    # Monitor management
    arandr
    autorandr
    # Power settings
    xfce.xfce4-power-manager
    # Authentication dialogs
    polkit_gnome
    # User dirs
    xdg-user-dirs
  ];

  # For Gnome software
  programs.dconf.enable = true;

  # Display
  services = {
    # X11
    xserver = {
      enable = true;
      desktopManager.xterm.enable = lib.mkDefault false;
      windowManager.default = "i3";
      windowManager.i3.enable = true;
      windowManager.i3.package = pkgs.i3-gaps;
      displayManager.lightdm.enable = true;
      displayManager.lightdm.background = "${custompkgs.solus-wallpaper}/share/backgrounds/solus/LakeSideView.jpg";
      displayManager.lightdm.greeters.enso = {
        enable = true;
        iconTheme.name = "Papirus";
        theme.name = "Arc-Darker";
        theme.package = pkgs.arc-theme;
        cursorTheme.package = pkgs.breeze-icons;
        cursorTheme.name = "breeze_cursors";
      };
    };
    # Compton
    compton = {
      enable = true;
      backend = "glx";
      vSync = true;
      fade = true;
      fadeDelta = 1;
      fadeSteps = [ "0.01" "0.012" ];
      shadow = true;
      shadowOffsets = [ (-10) (-10) ];
      shadowOpacity = "0.22";
      # activeOpacity = "1.00";
      # inactiveOpacity = "0.92";
      settings = {
        shadow-radius = 12;
        # blur-background = true;
        # blur-background-frame = true;
        # blur-background-fixed = true;
        blur-kern = "7x7box";
        blur-strength = 320;
      };
      opacityRules = [
        # "100:class_g = 'Firefox'"
        # "100:class_g = 'Vivaldi-stable'"
        "100:class_g = 'VirtualBox Machine'"
        # Art/image programs where we need fidelity
        "100:class_g = 'Gimp'"
        "100:class_g = 'Inkscape'"
        "100:class_g = 'aseprite'"
        "100:class_g = 'krita'"
        "100:class_g = 'feh'"
        "100:class_g = 'mpv'"
        "100:class_g = 'Rofi'"
        "100:class_g = 'Peek'"
        "99:_NET_WM_STATE@:32a = '_NET_WM_STATE_FULLSCREEN'"
      ];
      # shadowExclude = [
      #   # Put shadows on notifications, the scratch popup and rofi only
      #   "! name~='(scratch|Dunst)$'"
      # ];
      settings.blur-background-exclude = [
        "window_type = 'dock'"
        "window_type = 'desktop'"
        "class_g = 'Rofi'"
        "_GTK_FRAME_EXTENTS@:c"
      ];
    };
  };

  # Sound
  sound.enable = true;
  hardware.pulseaudio.enable = true;

  ## Fonts
  fonts = {
    fonts = with pkgs; [
      font-awesome-ttf
      font-awesome_4
    ];
  };

  # Try really hard to get QT to respect my GTK theme.
  my.env.GTK_DATA_PREFIX = [ "${config.system.path}" ];
  my.env.QT_QPA_PLATFORMTHEME = "qt5ct";
  qt5 = { style = "gtk2"; platformTheme = "gtk2"; };
  services.xserver.displayManager.sessionCommands = ''
    export GTK2_RC_FILES="$XDG_CONFIG_HOME/gtk-2.0/gtkrc"
    source "$XDG_CONFIG_HOME"/xsession/*.sh
    xrdb -merge "$XDG_CONFIG_HOME"/xtheme/*
  '';
}
