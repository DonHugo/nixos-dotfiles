
{ lib, buildPythonPackage, fetchFromGitHub, }:

buildPythonPackage rec {
  name = "rofi-filemenu";
  version = "0.1.2";

  src = ./source;
  # propagatedBuildInputs = [ aiofiles pyyaml pystache ];

  meta = with lib; {
    description = "Use rofi to search files and directories with fd and open them with xdg-open.";
    platforms = platforms.linux;
    maintainers = with maintainers; [ roland ];
  };
}
