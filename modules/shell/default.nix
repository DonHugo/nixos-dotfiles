{ config, options, lib, pkgs, ... }:

with lib;

{
  imports = [
    ./direnv.nix
    ./fish.nix
  ];
}
