{ config, options, lib, pkgs, ... }:

with lib;

{
  options.modules.shell.fish = {
    enable = mkOption { type = types.bool; default = false; };
  };

  config = mkIf config.modules.shell.fish.enable {

    environment.systemPackages = with pkgs; [
      unstable.fish
      exa
      fzf
      bat
      file
    ];
  };
}
