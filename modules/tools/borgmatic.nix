{ config, options, lib, pkgs, ... }:

with lib;

let
  custompkgs = import <custompkgs> {};
in
{
  options.modules.tools.borgmatic = {
    enable = mkOption {
      type = types.bool;
      default = false;
    };
  };

  config = mkIf config.modules.tools.borgmatic.enable {
    environment.systemPackages = with pkgs; [
      borgbackup
      lz4
      zstd
      xz
      custompkgs.borgmatic
    ];
  };
}
