[
  (self: super: {
    linuxPackagesFor = kernel:
      (super.linuxPackagesFor kernel).extend
      (self': super': { xpadneo = self'.callPackage ./packages/xpadneo { }; });
  })

  (self: super:
    with super; {
      # Occasionally, "stable" packages are broken or incomplete, so access to the
      # bleeding edge is necessary, as a last resort.
      unstable = import <nixos-unstable> { config.allowUnfree = true; };
    })
]
