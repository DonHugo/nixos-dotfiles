{ stdenv, python3Packages, fetchFromGitHub, borgbackup, zstd }:

let custompkgs = import <custompkgs> { };

in python3Packages.buildPythonApplication rec {
  pname = "borgmatic";
  version = "1.5.2";

  src = fetchFromGitHub {
    owner = "witten";
    repo = "borgmatic";
    rev = "${version}";
    sha256 = "1y7xw53cp7gi9njp0i1k2l3q24hq7999rlagqdcpkd1s3l918vma";
  };

  doCheck = false;

  propagatedBuildInputs = [ borgbackup zstd ];

  pythonPath = [
    custompkgs.pykwalify
    python3Packages.colorama
    python3Packages.llfuse
    python3Packages.requests
    python3Packages.ruamel_yaml
    python3Packages.setuptools
  ];

  postInstall = ''
    mkdir -p $out/lib/systemd/system
    install -Dm644 -t $out/lib/systemd/system sample/systemd/*
    for file in \
        $out/lib/systemd/system/* \
      ; do
        substituteInPlace $file --replace "/root/.local" "$out"
      done
  '';

  meta = with stdenv.lib; {
    description =
      "Simple, configuration-driven backup software for servers and workstations.";
    homepage = "https://torsion.org/borgmatic";
    platforms = platforms.linux;
  };
}
