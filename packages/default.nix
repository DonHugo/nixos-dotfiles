{ system ? builtins.currentSystem }:

let
  pkgs = import <nixos> {
    inherit system;
    config.allowUnfree = true;
  };

  callPackage = pkgs.lib.callPackageWith (pkgs // self);

  self = {
    # borgmatic
    borgmatic = callPackage ./borgmatic { };

    # breeze cursors
    xcursor-breeze = (callPackage ./xcursor-breeze { });

    # fonts
    fonts-meta = callPackage ./fonts-meta { };
    ttf-courier-prime = callPackage ./ttf-courier-prime { };
    ttf-clear-sans = callPackage ./ttf-clear-sans { };
    ttf-droid = callPackage ./ttf-droid { };
    ttf-gsfonts = callPackage ./ttf-gsfonts { };
    ttf-heuristica = callPackage ./ttf-heuristica { };
    ttf-impallari-cantora = callPackage ./ttf-impallari-cantora { };
    ttf-iosevka-ss08 = callPackage ./ttf-iosevka-ss08 { };
    ttf-merriweather = callPackage ./ttf-merriweather { };
    ttf-merriweather-sans = callPackage ./ttf-merriweather-sans { };
    ttf-oswald = callPackage ./ttf-oswald { };
    ttf-quintessential = callPackage ./ttf-quintessential { };
    ttf-signika = callPackage ./ttf-signika { };

    # hplip
    hplipWithPlugin = (callPackage ./hplip { });

    # hyphen
    hyphen-de = (callPackage ./hyphen/hyphen-de.nix { });

    kpeoplevcard = pkgs.libsForQt5.callPackage ./kpeoplevcard { };

    kdeconnect = pkgs.libsForQt5.callPackage ./kdeconnect { };

    kshutdown = pkgs.libsForQt5.callPackage ./kshutdown { };

    # kvantum
    kvantum = pkgs.libsForQt5.callPackage ./kvantum { };
    # latte-dock
    latte-applet-spacer = callPackage ./latte-applet-spacer { };
    latte-applet-separator = callPackage ./latte-applet-separator { };

    # materia theme
    materia-kde-theme = callPackage ./materia-kde-theme { };
    kvantum-theme-materia =
      callPackage ./materia-kde-theme/kvantum-theme-materia.nix { };

    # mythes
    mythes-de = (callPackage ./mythes/mythes-de.nix { });
    mythes-en = (callPackage ./mythes/mythes-en.nix { });

    pulseaudio-qt = pkgs.libsForQt5.callPackage ./pulseaudio-qt { };

    # pybase16-builder
    pybase16-builder = callPackage ./pybase16-builder { };

    # pykwalify
    pykwalify = callPackage ./pykwalify { };

    # vivaldi
    vivaldi = callPackage ./vivaldi {
      libX11 = pkgs.xorg.libX11;
      libXext = pkgs.xorg.libXext;
      libSM = pkgs.xorg.libSM;
      libICE = pkgs.xorg.libICE;
      libXfixes = pkgs.xorg.libXfixes;
      libXt = pkgs.xorg.libXt;
      libXi = pkgs.xorg.libXi;
      libXcursor = pkgs.xorg.libXcursor;
      libXScrnSaver = pkgs.xorg.libXScrnSaver;
      libXcomposite = pkgs.xorg.libXcomposite;
      libXdamage = pkgs.xorg.libXdamage;
      libXtst = pkgs.xorg.libXtst;
      libXrandr = pkgs.xorg.libXrandr;
      libXft = pkgs.xorg.libXft;
      libXrender = pkgs.xorg.libXrender;
      libxcb = pkgs.xorg.libxcb;
    };
    vivaldi-ffmpeg-codecs = callPackage ./vivaldi/ffmpeg-codecs.nix { };
    vivaldi-widevine = callPackage ./vivaldi/widevine.nix { };

    # wallpaper
    solus-wallpaper = callPackage ./solus-wallpaper/default.nix { };

    # xpadneo
    xpadneo = callPackage ./xpadneo { };
  };
in self
