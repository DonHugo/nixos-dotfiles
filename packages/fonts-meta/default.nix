{ stdenv, pkgs, lib, fetchurl }:

stdenv.mkDerivation rec {

  name = "fonts-meta";
  version = "1";

  src = fetchurl {
    url = "https://gist.github.com/cryzed/4f64bb79e80d619866ee0b18ba2d32fc/raw/bd073b52365393f9f0718425271825fc27b218f7/local.conf";
    sha256 = "1k6pjp0f9ji2pgfxa641bx8j7dszx0hsjwj7829p2n2a6ra1jab4";
  };

  unpackCmd = ''
    mkdir -p ./all
    cp -r $src ./all/30-infinality-aliases.conf
  '';

  installPhase = ''
    mkdir -p $out/etc/fonts/conf.d
    find . -type f -name \*.conf -exec cp {} $out/etc/fonts/conf.d \;
  '';

  meta = with lib; {
    description = "Extended font collection meta package, ported from Infinality (lite version)";
    platforms = platforms.linux;
    maintainers = with maintainers; [ roland ];
  };
}
