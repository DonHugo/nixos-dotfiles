{ stdenv, lib, fetchzip }:

stdenv.mkDerivation rec {

  name = "hyphen-de";
  version = "20060120";

  src1 = fetchzip {
    url = "https://www.mirrorservice.org/sites/download.openoffice.org/contrib/dictionaries/hyph_de_DE.zip";
    sha256 = "1qcjpx17n1581a6h0w30d5c9bi0i8iwpqy6dl9l0i3q8fi3fzv0q";
    stripRoot=false;
  };

  src2 = fetchzip {
    url = "https://www.mirrorservice.org/sites/download.openoffice.org/contrib/dictionaries/hyph_de_CH.zip";
    sha256 = "1izmxkym50mapyw4v6aflscbxi1pmwazksrf0c0wfgkwzhlx3q2l";
    stripRoot=false;
  };

  srcs = [ src1 src2 ];

  unpackCmd = ''
    mkdir -p ./all
    cp -r $curSrc/* ./all
  '';

  installPhase = ''
    install -dm755 "$out"/share/hyphen
    cp -p hyph_de_??.* "$out"/share/hyphen

    pushd "$out"/share/hyphen/
    de_DE_aliases="de_AT de_BE de_LU"
    for lang in $de_DE_aliases; do
        ln -s hyph_de_DE.dic hyph_$lang.dic
    done
    de_CH_aliases="de_LI"
    for lang in $de_CH_aliases; do
        ln -s hyph_de_CH.dic hyph_$lang.dic
    done
    popd

    # the symlinks
    install -dm755 "$out"/share/myspell/dicts
    pushd "$out"/share/myspell/dicts
    for file in "$out"/share/hyphen/*; do
        ln -sv $out/share/hyphen/$(basename $file) .
    done
    popd
  '';

  meta = with lib; {
    description = "German hyphenation rules";
    platforms = platforms.linux;
    maintainers = with maintainers; [ roland ];
  };
}
