{ mkDerivation
, lib
, fetchurl
, extra-cmake-modules
, kcmutils
, kconfigwidgets
, kdbusaddons
, kdoctools
, kiconthemes
, ki18n
, knotifications
, qca-qt5
, libfakekey
, libXtst
, qtx11extras
, sshfs
, makeWrapper
, kwayland
, kio
, qtmultimedia
, kirigami2
}:

let
  custompkgs = import <custompkgs> {};
in

mkDerivation rec {
  pname = "kdeconnect";
  version = "1.4";

  src = fetchurl {
    url = "mirror://kde/stable/${pname}/${version}/${pname}-kde-${version}.tar.xz";
    sha256 = "06i6spspqpl79x6z2bfvbgd08b3h1pyx5j1xjhd8ifyrm52pkvna";
  };

  buildInputs = [
    # New
    qtmultimedia
    custompkgs.kpeoplevcard
    custompkgs.pulseaudio-qt
    kirigami2
    #
    libfakekey libXtst
    ki18n kiconthemes kcmutils kconfigwidgets kdbusaddons knotifications
    qca-qt5 qtx11extras makeWrapper kwayland kio
  ];

  nativeBuildInputs = [ extra-cmake-modules kdoctools ];

  patches = [
    # MegamonkeyPatch!
    ./mypatch.patch
  ];


  postInstall = ''
    wrapProgram $out/libexec/kdeconnectd --prefix PATH : ${lib.makeBinPath [ sshfs ]}
  '';

  enableParallelBuilding = true;

  meta = with lib; {
    description = "KDE Connect provides several features to integrate your phone and your computer";
    homepage    = "https://community.kde.org/KDEConnect";
    license     = with licenses; [ gpl2 ];
    maintainers = with maintainers; [ fridh ];
  };
}
