{ mkDerivation, lib, extra-cmake-modules, kcoreaddons, ki18n, kitemviews
, kservice, kwidgetsaddons, qtbase, qtdeclarative, kpeople, kcontacts, fetchurl
}:

mkDerivation rec {
  pname = "kpeoplevcard";
  version = "0.1";

  src = fetchurl {
    url =
      "https://download.kde.org/stable/${pname}/${version}/${pname}-${version}.tar.xz";
    sha256 = "1hv3fq5k0pps1wdvq9r1zjnr0nxf8qc3vwsnzh9jpvdy79ddzrcd";
  };

  nativeBuildInputs = [ extra-cmake-modules ];

  buildInputs =
    [ kcoreaddons ki18n kitemviews kservice kwidgetsaddons qtdeclarative ];

  propagatedBuildInputs = [ kpeople kcontacts ];
}
