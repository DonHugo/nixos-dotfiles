{ mkDerivation, lib, extra-cmake-modules, knotifyconfig, kidletime
, hicolor-icon-theme, fetchzip }:

mkDerivation rec {
  pname = "kshutdown";
  version = "5.2";

  src = fetchzip {
    url =
      "https://downloads.sourceforge.net/${pname}/KShutdown/${version}/${pname}-source-${version}.zip";
    sha256 = "1nib6vm30wlm2a15cq85w0py8ia4zi2335vm87qsdp2wkc7bh4gw";
    extraPostFetch = ''
      chmod go-w $out
    '';
  };

  nativeBuildInputs = [ extra-cmake-modules ];

  propagatedBuildInputs = [ knotifyconfig kidletime hicolor-icon-theme ];

  enableParallelBuilding = true;

  meta = with lib; {
    description = "Shutdown Utility";
    homepage = "https://kshutdown.sourceforge.io";
    license = with licenses; [ gpl2 ];
  };
}
