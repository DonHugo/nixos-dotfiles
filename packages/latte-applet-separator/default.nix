{ stdenv, fetchFromGitHub }:

stdenv.mkDerivation rec {
  pname = "plasma5-applets-latte-separator";
  version = "0.1.1";

  src = fetchFromGitHub {
    owner = "psifidotos";
    repo = "applet-latte-separator";
    rev = "v${version}";
    sha256 = "0px6ybbfnfbz3xgb977qacz86xjfy42alphyyjldilzwkg89nc07";
  };

  phases = "installPhase";

  installPhase = ''
    _pkgdir="$out/share/plasma/plasmoids/org.kde.latte.separator"
    cd $src
    install -Dm644 metadata.desktop -t "$_pkgdir"
    find contents/ -type f -exec install -Dm644 "{}" "$_pkgdir/{}" \;
    '';

  meta = {
    description = "Plasma applet that acts as a separator between applets";
    license = stdenv.lib.licenses.gpl3;
    maintainers = [ stdenv.lib.maintainers.nixy ];
    platforms = stdenv.lib.platforms.all;
  };
}
