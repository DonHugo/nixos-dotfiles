{ stdenv, fetchFromGitHub }:

stdenv.mkDerivation rec {
  pname = "plasma5-applets-latte-spacer";
  version = "0.3.0";

  src = fetchFromGitHub {
    owner = "psifidotos";
    repo = "applet-latte-spacer";
    rev = "v${version}";
    sha256 = "03225jksp0h4cp07s58cb37k8bf93ird3dcbp32bp12jw7v7j7f9";
  };

  phases = "installPhase";

  installPhase = ''
    _pkgdir="$out/share/plasma/plasmoids/org.kde.latte.spacer"
    cd $src
    install -Dm644 metadata.desktop -t "$_pkgdir"
    find contents/ -type f -exec install -Dm644 "{}" "$_pkgdir/{}" \;
    '';

  meta = {
    description = "Plasma applet that acts as a spacer between applets";
    license = stdenv.lib.licenses.gpl3;
    maintainers = [ stdenv.lib.maintainers.nixy ];
    platforms = stdenv.lib.platforms.all;
  };
}
