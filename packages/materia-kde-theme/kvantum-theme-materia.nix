{ stdenv, fetchFromGitHub }:

stdenv.mkDerivation rec {
  pname = "kvantum-theme-materia";
  version = "20200312";

  src = fetchFromGitHub {
    owner = "PapirusDevelopmentTeam";
    repo = "materia-kde";
    rev = version;
    sha256 = "014f96dq28g7yw6mfmxm76x1p2j572pd9h7mkg8jvj89navaallg";
  };

  phases = "installPhase";

  installPhase = ''
    mkdir -p $out/share

    cp -r $src/Kvantum $out/share
    '';

  meta = {
    description = "A port of the materia theme for Plasma";
    homepage = https://git.io/materia-kde;
    license = stdenv.lib.licenses.gpl3;
    maintainers = [ stdenv.lib.maintainers.nixy ];
    platforms = stdenv.lib.platforms.all;
  };
}
