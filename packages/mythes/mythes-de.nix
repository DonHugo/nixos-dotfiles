{ stdenv, lib, fetchurl, unzip }:

stdenv.mkDerivation rec {

  name = "mythes-de";
  version = "20180514";

  src = fetchurl {
    url = "https://sources.archlinux.org/other/${name}/Deutscher-Thesaurus-${version}.oxt";
    sha256 = "1nhn26fyqkpcdkczadm6j593ih2ycx1xzri89dzrdn28h7w11bn3";
  };

  buildInputs = [ unzip ];

  phases = [ "unpackPhase" "installPhase" ];

  unpackPhase = ''
  unzip -q $src
  '';

  installPhase = ''
    install -dm755 "$out"/share/mythes
    cp -p th_de_DE_v2.* "$out"/share/mythes

    pushd "$out"/share/mythes/
    de_DE_aliases="de_AT de_BE de_CH de_LI de_LU"
    for lang in $de_DE_aliases; do
        ln -s th_de_DE_v2.idx "th_"$lang"_v2.idx"
        ln -s th_de_DE_v2.dat "th_"$lang"_v2.dat"
    done
    popd

    # the symlinks
    install -dm755 "$out"/share/myspell/dicts
    pushd "$out"/share/myspell/dicts
    for file in "$out"/share/mythes/*; do
        ln -sv $out/share/mythes/$(basename $file) .
    done
    popd
  '';

  meta = with lib; {
    description = "German thesaurus";
    platforms = platforms.linux;
    maintainers = with maintainers; [ roland ];
  };
}
