{ stdenv, lib, fetchzip }:

stdenv.mkDerivation rec {

  name = "mythes-en";
  version = "20060306";

  src = fetchzip {
    url = "https://www.mirrorservice.org/sites/download.openoffice.org/contrib/dictionaries/thes_en_US_v2.zip";
    sha256 = "12pd9372f205shvylz6d66ngrcxzz6hian0y5dz2a4k5y28v4pzm";
    stripRoot=false;
  };

  installPhase = ''
    install -dm755 "$out"/share/mythes
    cp -p th_en_US_v2.* "$out"/share/mythes

    pushd "$out"/share/mythes/
    en_US_aliases="en_AG en_AU en_BS en_BW en_BZ en_CA en_DK en_GB en_GH en_IE en_IN en_JM en_NA en_NG en_NZ en_PH en_SG en_TT en_ZA en_ZW"
    for lang in $en_US_aliases; do
        ln -s th_en_US_v2.idx "th_"$lang"_v2.idx"
        ln -s th_en_US_v2.dat "th_"$lang"_v2.dat"
    done
    popd

    # the symlinks
    install -dm755 "$out"/share/myspell/dicts
    pushd "$out"/share/myspell/dicts
    for file in "$out"/share/mythes/*; do
        ln -sv $out/share/mythes/$(basename $file) .
    done
    popd
  '';

  meta = with lib; {
    description = "English thesaurus";
    platforms = platforms.linux;
    maintainers = with maintainers; [ roland ];
  };
}
