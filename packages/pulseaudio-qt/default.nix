{
  mkDerivation, lib,
  extra-cmake-modules,
  kcoreaddons, ki18n, kitemviews, kservice, kwidgetsaddons, qtbase,
  doxygen,
  libpulseaudio, fetchurl
}:

mkDerivation rec {
  pname = "pulseaudio-qt";
  version = "1.2";

  src = fetchurl {
    url = "https://download.kde.org/stable/${pname}/${pname}-${version}.tar.xz";
    sha256 = "1i0ql68kxv9jxs24rsd3s7jhjid3f2fq56fj4wbp16zb4wd14099";
  };

  nativeBuildInputs = [ extra-cmake-modules ];

  buildInputs = [
    doxygen qtbase
  ];

  propagatedBuildInputs = [ libpulseaudio ];
}
