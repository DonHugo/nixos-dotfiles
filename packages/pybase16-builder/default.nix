{ stdenv, python3Packages }:

python3Packages.buildPythonPackage rec {
  pname = "pybase16-builder";
  version = "0.2.6";

  src = python3Packages.fetchPypi {
    inherit pname version;
    sha256 = "0i12wnhm9nys4cjcvf558brbyc2xnf3mnl3shnnvrck2zidwmci0";
  };

  propagatedBuildInputs = [ python3Packages.aiofiles
                            python3Packages.pyyaml
                            python3Packages.pystache ];

  meta = with stdenv.lib; {
    description = "Finally, a base16 builder that doesn’t require me to install anything new.";
    homepage = "https://github.com/InspectorMustache/base16-builder-python";
    platforms = platforms.linux;
  };
}
