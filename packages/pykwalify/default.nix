{ stdenv, python3Packages }:

python3Packages.buildPythonPackage rec {
  pname = "pykwalify";
  version = "1.7.0";

  src = python3Packages.fetchPypi {
    inherit pname version;
    sha256 = "7e8b39c5a3a10bc176682b3bd9a7422c39ca247482df198b402e8015defcceb2";
  };

  checkInputs = with python3Packages; [
    pytest
    testfixtures
  ];

  propagatedBuildInputs = with python3Packages; [
    docopt
    python-dateutil
    pyyaml
  ];

  meta = with stdenv.lib; {
    platforms = platforms.linux;
  };
}
