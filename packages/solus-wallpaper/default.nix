{ stdenv, lib, fetchFromGitHub }:

stdenv.mkDerivation rec {

  pname = "solus-wallpapers";
  version = "1";

  src = fetchFromGitHub {
    owner = "getsolus";
    repo = "artwork";
    rev = "5120697aef5aa22933dcba365fa0cb65aea7adbc";
    sha256 = "0f1z7mz1sgiblyqrwb5i88dgx2bhnzkv587dvfrvk6rvsvsqn4fd";
  };

  phases = [ "unpackPhase" "installPhase" ];

  installPhase = ''
    mkdir -p $out/share/backgrounds/solus

    find $src/backgrounds \( -name '*.jpg' -o -name '*.jepg' -o -name '*.png' \) -exec cp {} $out/share/backgrounds/solus \;

    # Plasma
    mkdir -p $out/share/wallpapers
    ln -sv $out/share/backgrounds/solus $out/share/wallpapers/solus
  '';

  meta = with lib; {
    description = "Background from Solus.";
    platforms = platforms.linux;
  };
}
