{ stdenv, lib, fetchzip, unzip }:

stdenv.mkDerivation rec {

  pname = "ttf-clear-sans";
  version = "1.00";

  src = fetchzip {
    stripRoot = false;
    url =
      "https://01.org/sites/default/files/downloads/clearsans-${version}.zip";
    sha256 = "0z89ws8kvsppisk0bc0ifpcz99j4138adr16z3rs991s9w7fb4ik";
  };

  installPhase = ''
    mkdir -p $out/share/fonts/clear-sans
    find . -type f -name \*.ttf -exec cp {} $out/share/fonts/clear-sans \;
  '';

  meta = with lib; {
    description = "A versatile OpenType font for screen, print and Web";
    platforms = platforms.linux;
  };
}
