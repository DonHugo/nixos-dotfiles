{ stdenv, lib, fetchzip, unzip }:

stdenv.mkDerivation rec {

  pname = "ttf-courier-prime";
  version = "1.203";

  courierPrimeSrc = fetchzip {
    stripRoot = false;
    url =
      "https://quoteunquoteapps.com/courierprime/downloads/courier-prime.zip";
    sha256 = "1m1lsdgdf3p5vvq1481lbxchwsych639v41jfz7r4jz04njmiyqb";
  };

  courierPrimeSansSrc = fetchzip {
    url =
      "https://quoteunquoteapps.com/courierprime/downloads/courier-prime-sans.zip";
    sha256 = "0y2sn28rl149qdhda5vfswg8y0rwajrk55krcyiwm3vkpsm31xk8";
  };

  courierPrimeCodeSrc = fetchzip {
    url =
      "https://quoteunquoteapps.com/courierprime/downloads/courier-prime-code.zip";
    sha256 = "15jdmwk2inhm921dilxfhhmyskds173v8rkrl93zvsd3qz8wml5z";
  };

  courierPrimeMediumSrc = fetchzip {
    stripRoot = false;
    url =
      "https://quoteunquoteapps.com/courierprime/downloads/courier-prime-medium-semi-bold.zip";
    sha256 = "15sflbj64wajcw9xvig51swaz4wx4xipzp91rmvbran86wninvs7";
  };

  srcs = [
    courierPrimeSrc
    courierPrimeSansSrc
    courierPrimeCodeSrc
    courierPrimeMediumSrc
  ];

  unpackCmd = ''
    mkdir -p ./all
    cp -r $curSrc ./all
  '';

  installPhase = ''
    mkdir -p $out/share/fonts/courier-prime
    find . -type f -name \*.ttf -exec cp {} $out/share/fonts/courier-prime \;
  '';

  meta = with lib; {
    description =
      "Monospace Courier font alternative optimized for screenplays";
    platforms = platforms.linux;
    maintainers = with maintainers; [ roland ];
  };
}
