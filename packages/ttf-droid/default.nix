{ stdenv, lib, fetchurl }:

stdenv.mkDerivation rec {

  name = "ttf-droid";
  version = "20121017";

  droidSrc = fetchurl {
    url = "https://sources.archlinux.org/other/community/${name}/${name}-${version}.tar.xz";
    sha256 = "1qv9h0m3nh29ylvj4yr68kf193f9a53ilnz73c6zw0c2kvnfys7h";
  };

  srcConf1 = ./60-droid-sans-mono.conf;
  srcConf2 = ./65-droid-sans.conf;
  srcConf3 = ./65-droid-serif.conf;
  srcConf4 = ./65-droid-kufi.conf;

  srcs = [ droidSrc srcConf1 srcConf2 srcConf3 srcConf4 ];


  unpackCmd = ''
    mkdir -p ./all
    ttfName=$(basename $(stripHash $curSrc))
    cp $curSrc ./all/$ttfName
  '';

  installPhase = ''
    mkdir -p $out/share/fonts/droid
    mkdir -p $out/etc/fonts/conf.d
    tar -xf *.tar.xz
    find . -type f -name \*.ttf -exec cp {} $out/share/fonts/droid \;
    find . -type f -name \*.conf -exec cp {} $out/etc/fonts/conf.d \;
  '';

  meta = with lib; {
    description = "A serif latin & cyrillic font, derived from the "Adobe Utopia" font by Apanov";
    platforms = platforms.linux;
    maintainers = with maintainers; [ roland ];
  };
}
