{ stdenv, lib, fetchFromGitHub }:

stdenv.mkDerivation rec {

  name = "gsfonts";
  version = "20180524";

  src = fetchFromGitHub {
    owner = "ArtifexSoftware";
    repo = "urw-base35-fonts";
    rev = "b758567463df532414c520cf008e27d9448b55a4";
    sha256 = "1k578r3qb0sjfd715jw0bc00pjvbrgnw0b7zrrhk33xghrdvp4r6";
  };

  installPhase = ''
    mkdir -p $out/share/fonts/gsfonts
    mkdir -p $out/etc/fonts/conf.d

    find . -type f -name \*.otf -exec cp {} $out/share/fonts/gsfonts \;
 	install -Dt $out/share/metainfo -m644 appstream/*.xml
    prefix="69-"
    for file in fontconfig/*.conf; do
        dir="$out"/etc/fonts/conf.d
        base=$(basename "$file")
        dest="$dir"/"$prefix""$base"
        mv "$file" "$dest"
    done
  '';

  meta = with lib; {
    description = "(URW)++ Core Font Set [Level 2]";
    platforms = platforms.linux;
    maintainers = with maintainers; [ roland ];
  };
}
