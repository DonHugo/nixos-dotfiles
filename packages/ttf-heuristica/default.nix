{ stdenv, lib, fetchurl }:

stdenv.mkDerivation rec {

  name = "ttf-heuristica";
  version = "1.0.2";

  src = fetchurl {
    url = "https://downloads.sourceforge.net/project/heuristica/heuristica-ttf-${version}.tar.xz";
    sha256 = "0gamj8dp4pfqxcy9k0qc97l5qjrgwbcc6dxnxbf9bla197j23gq8";
  };

  setSourceRoot = "sourceRoot=`pwd`";

  installPhase = ''
    mkdir -p $out/share/fonts/heuristica
    find . -type f -name \*.ttf -exec cp {} $out/share/fonts/heuristica \;
  '';

  meta = with lib; {
    description = "A serif latin & cyrillic font, derived from the "Adobe Utopia" font by Apanov";
    platforms = platforms.linux;
    maintainers = with maintainers; [ roland ];
  };
}
