{ stdenv, lib, fetchurl }:

stdenv.mkDerivation rec {

  name = "ttf-impallari-cantora";
  version = "1.001";

  src = fetchurl {
    url = "https://github.com/google/fonts/raw/master/ofl/cantoraone/CantoraOne-Regular.ttf";
    sha256 = "0fr1p343yvlmszvlsg5jbmbf9axvsvpizi9vwbbhz3xa5mm22h8a";
  };

  phases = "installPhase";

  installPhase = ''
    mkdir -p $out/share/fonts/impallari-cantora
    cp $src $out/share/fonts/impallari-cantora/$(basename $(stripHash $src))
  '';

  meta = with lib; {
    description = "Cantora ('Singer' in Spanish) is a friendly semi formal, semi condensed, semi sans serif, from Pablo Impallari";
    platforms = platforms.linux;
    maintainers = with maintainers; [ roland ];
  };
}
