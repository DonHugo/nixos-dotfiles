{ stdenv, fetchzip }:

stdenv.mkDerivation rec {
  pname = "iosevka-ss08";
  version = "2.3.3";

  src = fetchzip {
    url = "https://github.com/be5invis/Iosevka/releases/download/v${version}/iosevka-ss08-${version}.zip";
    sha256 = "114wphcwk8qvhcvck5p5a5v93p9phl2x7xnf5cqrpvkw734k1301";
    stripRoot = false;
  };

  phases = "installPhase";

  installPhase = ''
    mkdir -p $out/share/fonts/${pname}
    install -m644 $src/ttf/*.ttf $out/share/fonts/${pname}
    '';

  meta = {
    description = "A slender monospace typeface. Shape: Pragmata Pro";
    license = stdenv.lib.licenses.gpl3;
    maintainers = [ stdenv.lib.maintainers.nixy ];
    platforms = stdenv.lib.platforms.all;
  };
}
