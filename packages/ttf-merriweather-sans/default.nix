{ stdenvNoCC
, lib
, fetchFromGitHub
}:

stdenvNoCC.mkDerivation rec {
  pname = "ttf-merriweather-sans";
  version = "1.008";

  src = fetchFromGitHub {
    owner = "SorkinType";
    repo = "Merriweather-Sans";
    rev = "8a1b078e3aeec6aecc856c3422898816af9b9dc7";
    sha256 = "1f6a64bv4b4b1v3g2pgrzxcys8rk12wq6wfxamgzligcq5fxaffd";
  };

  installPhase = ''
    install -m444 -Dt $out/share/fonts/truetype/${pname} fonts/ttfs/*.ttf
    install -m444 -Dt $out/share/fonts/woff/${pname} fonts/woff/*.woff
    install -m444 -Dt $out/share/fonts/woff2/${pname} fonts/woff2/*.woff2
  '';

  meta = with lib; {
    description = "A sans-serif typeface that is pleasant to read on screens by Sorkin Type Co";
    platforms = platforms.linux;
    maintainers = with maintainers; [ roland ];
  };
}
