{ stdenv, lib, fetchurl }:

stdenv.mkDerivation rec {

  name = "ttf-oswald";
  version = "1.008";

  src1 = fetchurl {
    url = "https://github.com/google/fonts/raw/master/ofl/oswald/static/Oswald-Bold.ttf";
    sha256 = "1n72hjbyybwksy3g4cvdhh8zfbj42xslrrwvq45sp61k7fx71phc";
  };

  src2 = fetchurl {
    url = "https://github.com/google/fonts/raw/master/ofl/oswald/static/Oswald-ExtraLight.ttf";
    sha256 = "0jm2r4mr7559shm2y8rp83mczlwy11vcd91192dxyl2yd2libr7p";
  };

  src3 = fetchurl {
    url = "https://github.com/google/fonts/raw/master/ofl/oswald/static/Oswald-Light.ttf";
    sha256 = "0ckpzkfzg4d53r2lb7612czc5zdp2bcl2dzw6az0y0mb7mwna899";
  };

  src4 = fetchurl {
    url = "https://github.com/google/fonts/raw/master/ofl/oswald/static/Oswald-Medium.ttf";
    sha256 = "17qlmgz228v4wsqwfjjhbg97pi4gmpc6il6q42z3lza8g2q6zrwb";
  };

  src5 = fetchurl {
    url = "https://github.com/google/fonts/raw/master/ofl/oswald/static/Oswald-Regular.ttf";
    sha256 = "0vh060ckhfd4yirqddbafw2cxsnj8zv8alwa47ka3xdk9wbmmda3";
  };

  src6 = fetchurl {
    url = "https://github.com/google/fonts/raw/master/ofl/oswald/static/Oswald-SemiBold.ttf";
    sha256 = "0dw5h20ahn2zbj35xjnl7845xy79w32nycs7nhywmsw4wglj2r2d";
  };

  srcs = [ src1 src2 src3 src4 src5 src6 ];

  unpackCmd = ''
    mkdir -p ./all
    cp -r $curSrc ./all/$(basename $(stripHash $curSrc))
  '';

  installPhase = ''
    mkdir -p $out/share/fonts/oswald
    find . -type f -name \*.ttf -exec cp {} $out/share/fonts/oswald \;
  '';

  meta = with lib; {
    description = "Sans-serif typeface from Google by Vernon Adams";
    platforms = platforms.linux;
    maintainers = with maintainers; [ roland ];
  };
}
