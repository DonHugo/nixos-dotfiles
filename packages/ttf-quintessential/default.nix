{ stdenv, lib, fetchurl }:

stdenv.mkDerivation rec {

  name = "ttf-quintessential";
  version = "1.001";

  src = fetchurl {
    url = "https://github.com/google/fonts/raw/883939708704a19a295e0652036369d22469e8dc/ofl/quintessential/Quintessential-Regular.ttf";
    sha256 = "0clxh96z70g9s3bckb8nbm0i4vq3jgxjla5f98hidixz1pqr5lbk";
  };

  unpackCmd = ''
    mkdir -p ./all
    cp -r $src ./all/$(basename $(stripHash $src))
  '';

  installPhase = ''
    mkdir -p $out/share/fonts/quintessential
    find . -type f -name \*.ttf -exec cp {} $out/share/fonts/quintessential \;
  '';

  meta = with lib; {
    description = "Calligraphic typeface from Google by Brian J. Bonislawsky";
    platforms = platforms.linux;
    maintainers = with maintainers; [ roland ];
  };
}
