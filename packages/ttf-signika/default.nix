{ stdenv, lib, fetchurl }:

stdenv.mkDerivation rec {

  name = "ttf-signika";
  version = "1.002";

  src1 = fetchurl {
    url = "https://github.com/google/fonts/raw/d774fc0799fdeddbd8720616a6d2d791be29fafa/ofl/signika/Signika-Bold.ttf";
    sha256 = "142h3zn3zsnl5yb2l7bwg5cllz7xfk87ip4yq8r38hgr4mvw2r1c";
  };

  src2 = fetchurl {
    url = "https://github.com/google/fonts/raw/d774fc0799fdeddbd8720616a6d2d791be29fafa/ofl/signika/Signika-Light.ttf";
    sha256 = "1rvqg5ylgn5xrrdnzgs6dms460cdg5pipqz4kkyrdlrfxiiyzs4w";
  };

  src3 = fetchurl {
    url = "https://github.com/google/fonts/raw/d774fc0799fdeddbd8720616a6d2d791be29fafa/ofl/signika/Signika-Regular.ttf";
    sha256 = "1myiqv59lvli63sv23c5xr6j8l16xbyvl4zyxncalsvxqvdcv9nf";
  };

  src4 = fetchurl {
    url = "https://github.com/google/fonts/raw/d774fc0799fdeddbd8720616a6d2d791be29fafa/ofl/signika/Signika-SemiBold.ttf";
    sha256 = "06ldriczqph2bnhm1sf8hhwvr91g3qvp1rirzgmc1nyhz1sb4m66";
  };

  srcs = [ src1 src2 src3 src4 ];

  unpackCmd = ''
    mkdir -p ./all
    cp -r $curSrc ./all/$(basename $(stripHash $curSrc))
  '';

  installPhase = ''
    mkdir -p $out/share/fonts/signika
    find . -type f -name \*.ttf -exec cp {} $out/share/fonts/signika \;
  '';

  meta = with lib; {
    description = "Sans-serif typeface from Google by Anna Giedryś";
    platforms = platforms.linux;
    maintainers = with maintainers; [ roland ];
  };
}
