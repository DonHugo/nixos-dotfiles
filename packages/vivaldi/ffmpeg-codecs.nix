{ stdenv, fetchurl
, dpkg
}:

stdenv.mkDerivation rec {
  name = "chromium-codecs-ffmpeg";
  version = "81.0.4044.138";
  _ubuver="0ubuntu0";
  _ubudist="18.04.1";

  src = fetchurl {
    url = "http://security.ubuntu.com/ubuntu/pool/universe/c/chromium-browser/chromium-codecs-ffmpeg-extra_${version}-${_ubuver}.${_ubudist}_amd64.deb";
    sha256 = "11i4xbb0xkwam02810br4pm67hasahkw0dk8k08fcp9c0kjh7lf8";
  };

  buildInputs = [ dpkg ];

  unpackPhase = ''
    dpkg-deb -x $src .
    find .
  '';

  installPhase = ''
    install -vD usr/lib/chromium-browser/libffmpeg.so $out/lib/libffmpeg.so
  '';

  meta = with stdenv.lib; {
    description = "Additional support for proprietary codecs for Vivaldi";
    homepage    = "https://ffmpeg.org/";
    license     = licenses.lgpl21;
    maintainers = with maintainers; [ betaboon lluchs ];
    platforms   = [ "x86_64-linux" ];
  };
}
