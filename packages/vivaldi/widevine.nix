{ stdenv, fetchurl
, unzip, dpkg,
}:

stdenv.mkDerivation rec {
  name = "widevine";
  version = "4.10.1610.0";
  _chrome_ver="80.0.3987.100";

  src = fetchurl {
    url = "https://dl.google.com/linux/deb/pool/main/g/google-chrome-stable/google-chrome-stable_${_chrome_ver}-1_amd64.deb";
    sha256 = "0l7a8nkhq7qy8lrl20icr169fni1nvkvvxibqipzjzmj7hxdxbpr";
  };

  buildInputs = [ unzip dpkg ];

  unpackPhase = ''
    dpkg --fsys-tarfile ${src} | tar -x --no-same-permissions --no-same-owner
    mv -v opt/google/chrome/WidevineCdm WidevineCdm
  '';

  installPhase = ''
    install -vD WidevineCdm/manifest.json $out/share/google/chrome/WidevineCdm/manifest.json
    install -vD WidevineCdm/LICENSE $out/share/google/chrome/WidevineCdm/LICENSE
    install -vD WidevineCdm/_platform_specific/linux_x64/libwidevinecdm.so $out/share/google/chrome/WidevineCdm/_platform_specific/linux_x64/libwidevinecdm.so
  '';

  meta = with stdenv.lib; {
    description = "Widevine support for Vivaldi";
    homepage = "https://www.widevine.com";
    # license = licenses.unfree;
    maintainers = with maintainers; [ betaboon ];
    platforms   = [ "x86_64-linux" ];
  };
}
