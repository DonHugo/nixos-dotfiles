{ stdenv, lib, fetchurl }:

stdenv.mkDerivation rec {

  pname = "xcursor-breeze";
  version = "5.18.3";

  src = fetchurl {
    url = "http://download.kde.org/stable/plasma/${version}/breeze-${version}.tar.xz";
    sha256 = "19hn0a6mpb5ac3mc36bwvlk2mzgzax13dqqf9pgdirhpazilhl7p";
  };

  installPhase = ''
    install -dm755 "$out"/share/icons/
    cp -r cursors/Breeze/Breeze/ "$out"/share/icons/
    cp -r cursors/Breeze_Snow/Breeze_Snow/ "$out"/share/icons/
  '';

  meta = with lib; {
    description = "Breeze cursor theme (KDE Plasma 5). This package is for usage in non-KDE Plasma desktops.";
    platforms = platforms.linux;
  };
}
