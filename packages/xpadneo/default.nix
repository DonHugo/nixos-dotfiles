{ stdenv, fetchFromGitHub, kernel, kmod }:

stdenv.mkDerivation rec {
  pname = "xpadneo";
  version = "2020-04-09-${kernel.version}";

  src = fetchFromGitHub {
    owner = "atar-axis";
    repo = "xpadneo";
    rev = "3a846e5972337cc44d35d5f871463944ce935780";
    sha256 = "0jnj27l65qkagy3qn0p7f7asbmxv37vrrlps2drkq20is0frpnp4";
  };

  nativeBuildInputs = kernel.moduleBuildDependencies;

  preConfigure = ''
    cd hid-xpadneo/src
  '';

  buildPhase = ''
    make -C "${kernel.dev}/lib/modules/${kernel.modDirVersion}/build" "M=$PWD" modules
  '';

  installPhase = ''
    mkdir -p $out/lib/modules/${kernel.modDirVersion}/kernel/drivers/hid $out/lib/udev/rules.d
    cp hid-xpadneo.ko $out/lib/modules/${kernel.modDirVersion}/kernel/drivers/hid
    cp udev_rules/* $out/lib/udev/rules.d
    for i in $out/lib/udev/rules.d/*; do
      substituteInPlace "$i" \
        --replace \"/bin/sh \"${stdenv.shell} \
        --replace ' modprobe ' ' ${kmod}/bin/modprobe '
    done
  '';

  meta = with stdenv.lib; {
    maintainers = with maintainers; [ abbradar ];
    platforms = platforms.linux;
    description = "Advanced Linux Driver for Xbox One Wireless Controller (shipped with Xbox One S)";
    license = licenses.gpl3;
    homepage = "https://github.com/atar-axis/xpadneo";
  };
}
